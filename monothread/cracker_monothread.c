#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<crypt.h>

const char SALT[] = "$5$tresdur$";
const char TARGET[] = "$5$tresdur$.qNwwQxzGFIMpbOz.hHo72vNlbRO.7.nztwi6K4I3fA";

void main(int argc, char** args) {
   FILE *dic = fopen("../dictionary/yahoo.dic", "r");

   char* tmp = (char*) malloc(50*sizeof(char));
	char mdp[50]; 
   char* hash = (char*) malloc(50*sizeof(char));
   
   if(dic != NULL)
	   do {
		   fgets(tmp, 50, dic);
         strcpy(mdp, tmp);
         mdp[strlen(mdp)-2] = 0;
         strcpy(hash, crypt(mdp, SALT));
	   } while(!feof(dic) && strcmp(TARGET, hash) != 0);
	
   fclose(dic);

	printf("found : %s\n", mdp);
}

