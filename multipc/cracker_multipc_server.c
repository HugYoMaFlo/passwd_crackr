/**
    @author Antiphon Hugues
            Boulant Florian
            Chougui Mathieu
            Eichelberger Yoann
    @version 1.0 
*/
#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>
#include <string.h>
#include <signal.h>

int LINES_PER_SLAVE;

/**
 * Fonction permettant de calculer le nombre de lignes dans le fichier donné en paramètre
 */
int getNbLines(FILE *file)
{
  int nb_lines = 0;

  while (!feof(file))
    if (fgetc(file) == '\n')
      nb_lines++;

  return nb_lines;
}

/**
 * Point d'entrée du programme
 */
void main(int argc, char **args)
{
  //Cas où le programme n'est pas utilsié correctement (pas assez d'arguments).
  if (argc < 2)
  {
    printf("USAGE: donnez les adresses des esclaves\n");
    exit(1);
  }

  FILE *fp = fopen("../dictionary/yahoo.dic", "r");

  LINES_PER_SLAVE = getNbLines(fp) / (argc - 1);
  
  //On récupère le répertoire courant.
  char current_directory[1024];
  getcwd(current_directory, sizeof(current_directory));
  //ssh_command = (char *)realloc(ssh_command, 1000*sizeof(char));

  int i;

  //Création des différents processus qui vont effectuer les différents ssh
  for (i = 1; i < argc; i++)
  {
    if (!fork())
    {
      char *command = malloc(1000 * sizeof(char));
      sprintf(command, "cd %s; ./client.x %d %d", current_directory, LINES_PER_SLAVE, (i - 1) * LINES_PER_SLAVE);
      execlp("ssh", "ssh", args[i], "-t", "-t", command, NULL);
    }
  }
  
  //Récupération des codes de retour des différents processus fils et on termine le programme si jamais le mdp a été trouvé
  int status;
  for(i = 1 ; i < argc ; i++)
  {
    if( wait(&status) == -1 ) {
        perror("wait() failed");
        exit(EXIT_FAILURE);
    }
  
    int es = WEXITSTATUS(status); //récupération du code de retour
    if(es == 0)
      exit(0);
  }
}
