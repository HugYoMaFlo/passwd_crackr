# Bonjour.
Afin que vous viviez une expérience optimale, veuillez suivre les instructions suivantes.
Vous pouvez utiliser le Makefile afin de compiler tous les fichiers nécessaires au bon fonctionnement du programme.

## Instructions

```shell
$ make
$ ./server.x [liste des adresses IPs]
```

