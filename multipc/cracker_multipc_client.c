/**
    @author Antiphon Hugues
            Boulant Florian
            Chougui Mathieu
            Eichelberger Yoann
    @version 1.0
*/
#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <crypt.h>
#include <signal.h>
#include <pthread.h>

int NB_LINES_PER_THREAD;

// Définit le hash que l'on doit retrouver.
const char *TARGET = "$5$tresdur$.qNwwQxzGFIMpbOz.hHo72vNlbRO.7.nztwi6K4I3fA";
const char *SALT = "$5$tresdur$";

// Les informations sur les threads qui calculent.
pthread_t *THREADS;
int NB_THREADS;

// Le nombre de lignes à lire dans le fichier.
int NB_LINES;

// Rôle de booléen pour savoir si le mdp a été retrouvé ou non.
char found;
char *state;

/**
 * Fonction initialisant les informations liées aux threads.
 */
void initNbThreads()
{
  // On ouvre le flux résultant de l'exécution du script.
  FILE *fp = popen("./nb_threads.sh", "r");

  char *line = (char *)malloc(10 * sizeof(char));

  fgets(line, 10, fp);     // On récupère la donnée recherchée.
  NB_THREADS = atoi(line); // On la stocke dans la variable globale.

  // On initialise le tableau d'états des threads.
  state = (char *)malloc(NB_THREADS * sizeof(char));
  int i;
  for (i = 0; i < NB_THREADS; i++)
    state[i] = 0;

  // On ferme proprement le fichier.
  free(line);
  fclose(fp);
}

/**
 * Fonction permettant de placer un fichier sur une certaine ligne.
 */
void goToLine(FILE *f, int n)
{
  int i = 0;

  if (f == NULL)
    printf("null\n");

  // Compte le nombre de lignes.
  while (i < n)
    if (fgetc(f) == '\n')
      i++;
}

/**
 * Fonction comportement du thread ; elle recherche le mot de passe voulu dans la base.
 */
void mine(int *args)
{
  int id = args[0];
  int first_line = args[1];
  //On ouvre le fichier du dictionnaire et on le place à la ligne souhaitée
  FILE *fp = fopen("./../dictionary/yahoo.dic", "r");
  goToLine(fp, first_line);

  char not_equal;
  char line[50];

  //On crée la structure utilisée par le crypt_r
  struct crypt_data crypt_res;
  crypt_res.initialized = 0;

  int i = 0;

  //Boucle de 'minage' : on teste de crypter chaque ligne du dictionnaire de mot de passe et on vérifie si c'est égal à celui recherché
  int length;
  do
  {
    fgets(line, 50, fp);
    length = strlen(line);
    if (line[length - 2] == 13) // 13 est le code ASCII du 'carriage return', le caractère \n
      line[length - 2] = 0;
    i++;
  } while (i < NB_LINES_PER_THREAD && (not_equal = strcmp(TARGET, crypt_r(line, SALT, &crypt_res))));

  if (not_equal)
    state[id] = 1;
  else
  {
    found = 1;
    printf("found ! %s\n", line); // LOG
  }

  fclose(fp);

  pthread_exit(NULL);
}

/**
 * Fonction vérifiant si tous les threads se sont terminés sans trouver quoi que ce soit.
 */
char checkThreads()
{
  int i;
  char result = 0;

  for (i = 0; i < NB_THREADS; i++)
    if (state[i] == 0)
      result = 1;

  return result;
}

/**
 * Point d'entrée du programme
 */
void main(int argc, char **args)
{
  if (argc != 3)
  {
    printf("USAGE: %s nb_lines start_line\n", args[0]);
    exit(1);
  }

  //On initialise les données liées aux THREADS
  initNbThreads();

  int first_line = atoi(args[2]);
  pthread_t tab_threads[NB_THREADS];

  //On calcule le nombre de lignes à parcourir par thread
  NB_LINES_PER_THREAD = atoi(args[1]) / NB_THREADS;

  // On créé les threads et on leur assigne un id et la ligne à laquelle ils doivent commencer à chercher.
  int i;
  for (i = 0; i < NB_THREADS; i++)
  {
    // Initialise les arguments pour chaque threads.
    int *args = (int *)malloc(sizeof(int) * 2);
    args[0] = i;
    args[1] = i * NB_LINES_PER_THREAD + first_line;

    pthread_create(&tab_threads[i], NULL, (void *)mine, args);
    pthread_detach(tab_threads[i]);
  }

  //moniteur : tant que les conditions d'arrêt ne sont pas rencontrées, on tourne
  while (checkThreads() && !found)
    usleep(5);

  //Si le mot de passe a été trouvé, on renvoie 0. Sinon, on renvoie 1
  exit(!found);
}
