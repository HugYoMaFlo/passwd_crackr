#define _GNU_SOURCE
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include<crypt.h>
#include<signal.h>
#include<pthread.h>

const char* TARGET = "$5$tresdur$.qNwwQxzGFIMpbOz.hHo72vNlbRO.7.nztwi6K4I3fA";
int NB_LINES;

int getNbThreads() {
   FILE *fp;

   fp = popen("./nb_threads.sh", "r");

   char *line = (char*) malloc(10*sizeof(char));
   int nb_threads;

   fgets(line, 10, fp);
   nb_threads = atoi(line);

   free(line);
   fclose(fp);

   return nb_threads;
}

int getNbLines(FILE *file) {
   int nb_lines = 0;

   while(!feof(file))
      if(fgetc(file) == '\n')
         nb_lines++;

   return nb_lines;
}

void goToLine(FILE *f, int n) {
   int i=0;

   while(i<n)
      if(fgetc(f) == '\n')
         i++;
}

void mine(int* first_line) {
   FILE *fp = fopen("../dictionary/yahoo.dic", "r");
   goToLine(fp, *first_line);

   char not_equal;
   char line[50];

   struct crypt_data crypt_res;
   crypt_res.initialized = 0;

   for(int i=0; i < NB_LINES && (not_equal = strcmp(TARGET, crypt_r(line, "$5$tresdur$", &crypt_res) ) ); i++) {
      fgets(line, 50, fp);
      line[strlen(line)-2] = 0;
   }

   if(!not_equal) {
      printf("found ! %s\n", line);
      raise(SIGKILL);
   }
}


void main(int argc, char** args) {
   FILE *fp = fopen("../dictionary/yahoo.dic", "r");

   int nb_threads = getNbThreads();
   NB_LINES = getNbLines(fp);

   fclose(fp);

   pthread_t tab_threads[nb_threads];  

   int i;

   for(i=0; i < nb_threads; i++) {
      int* first_line = (int *) malloc(sizeof(int));
      *first_line = i*(NB_LINES/nb_threads);
      pthread_create(&tab_threads[i], NULL, (void *)mine, first_line);
   }

   for(i=0; i < nb_threads; i++)
      pthread_join(tab_threads[i], NULL);
}
