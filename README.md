# PASSWD CRACKR

## Auteurs
* ANTIPHON Hugues
* BOULANT Florian
* CHOUGUI Mathieu
* EICHELBERGER Yoann

## Enoncé
On souhaite vérifier si les mots de passe des utilisateurs d'un serveur sont suffisamment robustes et n'appartiennent pas à un dictionnaire de mots.
On utilisera la puissance de plusieurs machines afin de réduire le temps de recherche.

## Instructions
Pour ce faire, écrivez un programme en langage C qui teste si le hash 
$5$tresdur$.qNwwQxzGFIMpbOz.hHo72vNlbRO.7.nztwi6K4I3fA
fait partie du dictionnaire fourni (quelques mots de passe de Yahoo !) en parallélisant les calculs sur plusieurs ordinateurs (ssh) et en utilisant le multi-threading.

### Spécifications
Attention, la fonction crypt qui permet de crypter un mot n'est pas ré-entrante.
$5$ est le numéro de code de l'algorithme de cryptage utilisé.
tresdur en est le paramètre permettant d'en augmenter l'entropie.

## Structure
Afin que l'on puisse constater de l'évolution du projet, plusieurs dossiers sont à disposition et contiennent les différentes étapes de développement.
* monothread : le simple algorithme de recherche du mot de passe.
* multithread : la version utilisant le plein potentiel de la machine en répartissant les calculs sur plusieurs threads
* multipc : la version utilisant la puissance de calcul de plusieurs machines via le protocole SSH

La version finale se trouve être celle du répertoire multipc.
